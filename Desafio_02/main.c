#include <stdio.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

void send(__attribute__((unused)) int a){
    //Deals with the variable
}

int main()
{
clock_t average_t = 0;
    // Shitty code
    for(int j = 0; j < 1000; j++){

        clock_t start_t, end_t, total_t;
        start_t = clock();

        int data[8] = {0};
        int counter = 0;
        int accumulator = 0;
        int average = 0;

        for(int i=0; i<400; i++){
            int r = rand() % 180;
            data[i%8] = cos(r)*r + r*r;
            accumulator += cos(r)*r + r*r;
            counter++;

            average = accumulator / counter;
            send(average);
        }

        end_t = clock();

        total_t = (end_t - start_t);

        //printf("Total number of clock cycles: %d %d %d\n", start_t, end_t,total_t);
        //printf("Total number of clock cycles: %d\n", total_t);
        average_t += total_t;

    }

    printf("Total number of clock cycles: %lu\n", average_t/1000);

    // Enhanced code

    average_t = 0;
    int r2 = 0;
    for(int j = 0; j < 1000; j++){
        clock_t start_t, end_t, total_t;
        start_t = clock();

        int data[8] = {0};
        int counter = 0;
        int accumulator = 0;
        int average = 0;

        for(int i=0; i<400; i++){
            int r = rand() & 127;
            r2 = r*r;
            data[i&7] = r*((1 + (r2>>1) + ((r2*r2)>>2)) + r);
            accumulator += data[i&7];
            counter++;

            if((i&7) == 7){
               average = accumulator / counter;
               send(average);
            }
        }

        end_t = clock();

        total_t = (end_t - start_t);

        //printf("Total number of clock cycles: %d %d %d\n", start_t, end_t,total_t);
        //printf("Total number of clock cycles: %d\n", total_t);
        average_t += total_t;

    }
    printf("Total number of clock cycles (improved): %lu\n", average_t/1000);

    return 0;
}
