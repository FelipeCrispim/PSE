# Programação para Sistemas Embarcados

## Melhorias no código

* Módulo (%)
	Substituído por AND (&), uma vez que o divisor está na base 2.

* Média
	É feito um armazenamento e somatório, da quantidade e valor das amostras.
	Então é feito a média quando ela se torna necessária.

* Polinômio
	Isola o fator comum.

* Série de taylor
	Usado para substituir a função cosseno -que é mais custosa principalmente por conta de sua precisão- .

* Dividir por constante
	Substituído pelo shift (>>), usado por exemplo: para x ≥ 0, x >> n = x / 2^n.

## Desempenho
	Houve uma melhoria de aproximadamente 80%.