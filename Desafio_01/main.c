#include <stdio.h>
#include "circular_buffer.h"
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main()
{
    int sizeStruct = 20;
    int data[sizeStruct];
    struct circ_buff myBuffer;
    initBuffer(&myBuffer, data, sizeStruct);
    srand(time(NULL));
    int milliseconds = 100;

    for(int x =0; x <= sizeStruct; x++) {
        int r = rand()%100;
        if(addElement(&myBuffer, r) == 0){

            //Buffer limit
            int newArray[sizeStruct];
            memset(newArray, 0, sizeof(newArray));
            memcpy(newArray, myBuffer.cells, sizeStruct*sizeof(int));
            for(int y =0; y < sizeStruct ; y++) {
                printf("value: %d, hexa: %x\n", newArray[y], myBuffer.cells[y]);
            }
        }
        usleep(milliseconds*1000);
    }
    printf("saiu do for\n");

    return 0;
}
