#include "circular_buffer.h"
#include <stdlib.h>
#include <string.h>

void initBuffer(struct circ_buff *myBuffer, int * arr, int size)
{
    myBuffer->cells = arr;
    myBuffer->size = size;
    myBuffer->head = 0;
    myBuffer->tail = 0;
    myBuffer->content = 0;
}

int addElement(struct circ_buff *buff, int value)
{
    if(buff->content < buff->size){
        buff->cells[buff->tail] = value;
        buff->content += 1;
        if(buff->tail <= buff->size-1) {
            buff->tail += 1;
        } else {
            buff->tail = 0;
        }

        return 1;
    } else {
        return 0;
    }
}

void removeElement(struct circ_buff *buff)
{
    if(buff->content > 0){
        buff->content -= 1;
        if(buff->head < buff->size-1) {
            buff->head += 1;
        } else {
            buff->head = 0;
        }
    }
}
