#include <stdio.h>

#define BUFFSIZE 20

struct circ_buff{
    int * cells;
    int head;
    int tail;
    int size;
    int content;
};

void initBuffer(struct circ_buff *myBuffer, int *arr, int size);

//int nextElement(int elem, int limit);

int addElement(struct circ_buff *buff, int value);

void removeElement(struct circ_buff *buff);
